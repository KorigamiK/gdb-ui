import { useState } from "react";
import { invoke } from "@tauri-apps/api/tauri";
export default function Home() {
  const [greetMsg, setGreetMsg] = useState("");
  const [name, setName] = useState("");

  async function greet() {
    setGreetMsg(await invoke("greet", { name }));
  }

  return (
    <div className="flex flex-row h-full w-full m-10">
      <div>
        <div className="flex flex-col">
          <h1>Welcome to GDB-UI!</h1>

          <form
            className="flex flex-row pt-5 gap-2"
            onSubmit={(e) => {
              e.preventDefault();
              greet();
            }}
          >
            <input
              id="greet-input"
              onChange={(e) => setName(e.currentTarget.value)}
              placeholder="Enter a name..."
            />
            <button type="submit">Greet</button>
          </form>

          <p className="mt-4 text-xl">{greetMsg}</p>
        </div>
      </div>
    </div>
  );
}
